Name:                libtcnative-1-0
Version:             1.2.39
Release:             2
Summary:             JNI wrappers for Apache Portable Runtime for Tomcat
License:             Apache-2.0
Url:                 https://tomcat.apache.org/tomcat-7.0-doc/apr.html
Source0:             https://archive.apache.org/dist/tomcat/tomcat-connectors/native/%{version}/source/tomcat-native-%{version}-src.tar.gz
Source1:             https://archive.apache.org/dist/tomcat/tomcat-connectors/native/%{version}/source/tomcat-native-%{version}-src.tar.gz.asc
# https://www.apache.org/dist/tomcat/tomcat-connectors/KEYS
Source2:             %{name}.keyring 

BuildRequires:       fdupes java-1.8.0-openjdk-devel javapackages-tools apr-devel gcc
BuildRequires:       openssl-devel >= 1.0.2 pkgconfig
Provides:            tcnative = %{version}
Provides:            tomcat-native = %{version}
Obsoletes:           tomcat-native < %{version}

%description
Tomcat can use the Apache Portable Runtime to provide superior
scalability, performance, and better integration with native server
technologies. The Apache Portable Runtime is a highly portable library
that is at the heart of Apache HTTP Server 2.x. APR has many uses,
including access to advanced IO functionality (such as sendfile, epoll
and OpenSSL), OS level functionality (random number generation, system
status, etc), and native process handling (shared memory, NT pipes and
Unix sockets).
These features allows making Tomcat a general purpose webserver, will
enable much better integration with other native web technologies, and
overall make Java much more viable as a full fledged webserver platform
rather than simply a backend focused technology.

%package devel
Summary:             JNI wrappers for Apache Portable Runtime for Tomcat
Requires:            %{name} = %{version}-%{release} glibc-devel apr-devel openssl-devel
Conflicts:           libtcnative < 1.2.39-2

%description devel
Tomcat can use the Apache Portable Runtime to provide superior
scalability, performance, and better integration with native server
technologies. The Apache Portable Runtime is a highly portable library
that is at the heart of Apache HTTP Server 2.x. APR has many uses,
including access to advanced IO functionality (such as sendfile, epoll
and OpenSSL), OS level functionality (random number generation, system
status, etc), and native process handling (shared memory, NT pipes and
Unix sockets).
These features allows making Tomcat a general purpose webserver, will
enable much better integration with other native web technologies, and
overall make Java much more viable as a full fledged webserver platform
rather than simply a backend focused technology.

%prep
%setup -q -n tomcat-native-%{version}-src

%build
cd native
%configure \
    --with-apr=%{_bindir}/apr-1-config \
    --with-java-home=%{java_home} \
    --with-java-platform=2
%make_build

%install
%make_install -C native
%delete_la
install -d -m 755 %{buildroot}/%{_includedir}
install -m 644 native/include/* %{buildroot}/%{_includedir}

%files
%license LICENSE
%doc CHANGELOG.txt NOTICE README.txt
%{_libdir}/libtcnative-1.so.*

%files devel
%{_includedir}/*
%{_libdir}/libtcnative-1.so

%changelog
* Tue Dec 24 2024 Funda Wang <fundawang@yeah.net> - 1.2.39-2
- cleanup spec

* Thu Aug 01 2024 Ge Wang <wang__ge@126.com> - 1.2.39-1
- Upgrade to 1.2.39

* Wed Apr 12 2023 xu_ping <707078654@qq.com> - 1.2.36-1
- Upgrade to 1.2.36

* Wed Mar 08 2023 xu_ping <xuping33@h-partners.com> - 1.2.23-6
- change Buildrequires from java-devel to java-1.8.0-openjdk-devel.

* Wed Nov 10 2021 caodongxia <caodongxia@huawei.com> - 1.2.23-5
- Obsoletes tomcat-native

* Tue Oct 26 2021 chenchen <chen_aka_jan@163.com> - 1.2.23-4
- change the spec file name to be the same as the repo name

* Tue Jun 08 2021 wulei <wulei80@huawei.com> - 1.2.23-3
- fixes failed: error: C compiler cannot create executables

* Sun Sep 27 2020 huanghaitao <huanghaitao8@huawei.com> - 1.2.23-2
- Delete tomcat-native provides to fix confilct

* Fri Jul 17 2020 leiju <leiju4@huawei.com> - 1.2.23-1
- Modify BuildRequires and Requires

* Sun Jun 28 2020 leiju <leiju4@huawei.com> - 1.2.23-0
- Package init
