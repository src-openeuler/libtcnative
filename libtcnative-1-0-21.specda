#
# spec file for package libtcnative-1-0
#
# Copyright (c) 2019 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#
%global JAVAPATH /usr/lib/jvm

Name:           libtcnative-1-0
Version:        1.2.23
Release:        2.2
Summary:        JNI wrappers for Apache Portable Runtime for Tomcat
License:        Apache-2.0
Group:          Productivity/Networking/Web/Servers
Url:            https://tomcat.apache.org/tomcat-7.0-doc/apr.html
Source0:        https://www.apache.org/dist/tomcat/tomcat-connectors/native/%{version}/source/tomcat-native-%{version}-src.tar.gz
Source1:        https://www.apache.org/dist/tomcat/tomcat-connectors/native/%{version}/source/tomcat-native-%{version}-src.tar.gz.asc
# https://www.apache.org/dist/tomcat/tomcat-connectors/KEYS
Source2:        %{name}.keyring
BuildRequires:  fdupes
BuildRequires:  java-devel
BuildRequires:  javapackages-tools
BuildRequires:  libapr1-devel >= 1.4.3
BuildRequires:  openssl-devel >= 1.0.2
BuildRequires:  pkgconfig
# Upstream compatibility:
Provides:       tcnative = %{version}
#Fedora compatibility
Provides:       tomcat-native = %{version}

%description
Tomcat can use the Apache Portable Runtime to provide superior
scalability, performance, and better integration with native server
technologies. The Apache Portable Runtime is a highly portable library
that is at the heart of Apache HTTP Server 2.x. APR has many uses,
including access to advanced IO functionality (such as sendfile, epoll
and OpenSSL), OS level functionality (random number generation, system
status, etc), and native process handling (shared memory, NT pipes and
Unix sockets).

These features allows making Tomcat a general purpose webserver, will
enable much better integration with other native web technologies, and
overall make Java much more viable as a full fledged webserver platform
rather than simply a backend focused technology.

%package devel
Summary:        JNI wrappers for Apache Portable Runtime for Tomcat
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}-%{release}
Requires:       glibc-devel
Requires:       libapr1-devel
Requires:       openssl-devel

%description devel
Tomcat can use the Apache Portable Runtime to provide superior
scalability, performance, and better integration with native server
technologies. The Apache Portable Runtime is a highly portable library
that is at the heart of Apache HTTP Server 2.x. APR has many uses,
including access to advanced IO functionality (such as sendfile, epoll
and OpenSSL), OS level functionality (random number generation, system
status, etc), and native process handling (shared memory, NT pipes and
Unix sockets).

These features allows making Tomcat a general purpose webserver, will
enable much better integration with other native web technologies, and
overall make Java much more viable as a full fledged webserver platform
rather than simply a backend focused technology.

%prep
%setup -q -n tomcat-native-%{version}-src

%build
#rm -rf %{JAVAPATH}/include
#mkdir %{JAVAPATH}/include
#cp %{JAVAPATH}/linux/jni_md.h %{JAVAPATH}/include
rpm -qa | grep java-1.8.0-openjdk-1.8.0 > javaname
for x in $(cat javaname); do
JAVAHOME=$x
done

cd native
%configure \
    --with-apr=%{_bindir}/apr-1-config \
    --with-java-home=%{JAVAPATH}/$JAVAHOME \
    --with-java-platform=2 \
    --with-os-type=linux

make %{?_smp_mflags}

%install
make -C native install DESTDIR=%{buildroot}
install -d -m 755 %{buildroot}/%{_includedir}
install -m 644 native/include/* %{buildroot}/%{_includedir}
find %{buildroot} -type f -name "*.la" -delete -print

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc CHANGELOG.txt LICENSE NOTICE README.txt
%{_libdir}/libtcnative-1.so.*
#bnc#622430 - java expects so files installed
%{_libdir}/libtcnative-1.so

%files devel
%{_includedir}/*
%{_libdir}/pkgconfig/*.pc

%changelog
* Thu Nov  7 2019 Matei Albu <malbu@suse.com>
- Add GPG keyring.
* Mon Aug 12 2019 Matei Albu <malbu@suse.com>
- Version update to version 1.2.23:
  * See changelog.html for in-depth upstream changes
* Thu Jun  6 2019 Matei <malbu@suse.com>
- Version update to version 1.2.21:
  * See changelog.html for in-depth upstream changes
  * Fix incompatibility with Tomcat (bsc#1130843)
* Mon Nov 27 2017 fstrba@suse.com
- Version update to version 1.2.16:
  * See changelog.html for in-depth upstream changes
  * Fixes build breakage with newer version of openssl
* Wed Feb  3 2016 tchvatal@suse.com
- Version update to version 1.2.4:
  * See changelog.html for in-depth upstream changes
  * This connector to properly work requires openssl 1.0.2 or newer
    so do not backport to other codestreams.
* Wed Feb 25 2015 tchvatal@suse.com
- Remove keyring file as there is new keyring and I didn't find it
  on the web
* Thu Feb 19 2015 p.drouand@gmail.com
- Update to version 1.1.32
  * Fix: 53952: Add support for TLSv1.2 and TLSv1.1.
  * Fix: 56844: Use OpenSSL 1.0.1j with Windows binaries.
  * Update: Use APR 1.5.1 with Windows binaries
- Remove tomcat-native-nosslv2.patch; merged on upstream release
- Remove %%gpg_verify tag and gpg-offline require; let OBS handles
  gpg verification
* Sun Apr 27 2014 crrodriguez@opensuse.org
- version 1.1.30
  * Fixed double-free in ssl_ocsp_request. Patch provided by
  Aristotelis.
  * Other minor bugfixes.
- openSUSE: Fix build when openssl does not have SSLv2 support.
  (tomcat-native-nosslv2.patch)
* Fri Sep 13 2013 mvyskocil@suse.com
- Update to 1.1.27 (bugfix release)
  * fix high CUP usage on client's IP address change
  * add CPU information to OS info for Linux
  * fix FIPS mode for listeners; resolves 'Low level API
    call to digest MD5 forbidden in FIPS mode!' errors.
  * update add clearOptions function to allow access to
    OpenSSL's SSL_CTX_clear_options function.
  * fix regression in pollset return value.
- add gpg verification
- add javapackages-tools
- drop config-guess-sub-update.patch
* Sun Mar 31 2013 schwab@suse.de
- config-guess-sub-update.patch: update config.guess/sub for aarch64
* Thu Dec  6 2012 kruber@zib.de
- update to 1.1.24
  * add support for per-socket timeouts inside poller
* Thu Mar 15 2012 mvyskocil@suse.cz
- update to 1.1.23 - latest upstream version
  * autodetect java7
  * better support for ipv6
  * OCSP verification support
  * explicit use in FIPS mode allowed
  * and fixes many bugs, leaks and crashes
- split the spec from tomcat6 sources as it was never needed
* Thu Aug  5 2010 mvyskocil@suse.cz
- fixes bnc#622430 - move .so file to main package
* Tue Mar 16 2010 ro@suse.de
- build from tomcat-native-1.1.20-src.tar.gz
- package needs work, does not have to live in tomcat src any more
* Wed Jun  3 2009 mvyskocil@suse.cz
- Tomcat update to 6.0.20
- APR update to 1.3.3 - the bugfix release
* Fri Sep 12 2008 mvyskocil@suse.cz
- Tomcat update to 6.0.18
* Thu Aug  7 2008 mvyskocil@suse.cz
- move the .so file to -devel subpackage to prevent of an rpmlint error
* Wed Jul  9 2008 mvyskocil@suse.cz
- The first release in SUSE (1.2.12)
  - fix of enhancenment request [bnc#202339]
